import psycopg2

class ConexionDB:
    def _init_(self, dbname, user, password, host, port):
        self.dbname = dbname
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        self.conn = None
        self.cursor = None
    
    def conectar(self):
        try:
            self.conn = psycopg2.connect(
                dbname=self.dbname,
                user=self.user,
                password=self.password,
                host=self.host,
                port=self.port
            )
            self.cursor = self.conn.cursor()
            print("Conexión exitosa")
        except Exception as e:
            print(f"Error de conexión: {e}")
    
    def desconectar(self):
        try:
            if self.cursor:
                self.cursor.close()
        except AttributeError:
            pass
        try:
            if self.conn:
                self.conn.close()
                print("Conexión cerrada")
        except AttributeError:
            pass