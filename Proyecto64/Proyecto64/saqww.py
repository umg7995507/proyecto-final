from flask import Flask, render_template, request, redirect, url_for
app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        role = request.form['roles']
        if role == 'estudiante':
            return redirect(url_for('estudiante'))
        if role == 'profesor':
            return redirect(url_for('profesor'))
        if role == 'director':
            return redirect(url_for('director'))
    return render_template('index.html')

@app.route('/estudiante')
def estudiante():
    return render_template('estudiante.html')

@app.route('/profesor')
def profesor():
    return render_template('profesor.html')

@app.route('/director')
def director():
    return render_template('director.html')

if __name__ == '__main__':
    app.run(debug=True)
    