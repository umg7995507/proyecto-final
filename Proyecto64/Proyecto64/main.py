from database import ConexionDB;

if (__name__) == "_main_":
    # Crear una instancia de la clase y conectar a la base de datos
    insercion = ConexionDB('proyecto64', 'postgres', 'jbaaf55535565','localhost', '5432')
    insercion.conectar()

    # Datos a insertar en la tabla
    datos = {
        'Nombres': 'Juan',
        'Apellidos': 'Pérez',
    }

    # Nombre de la tabla en la que se insertarán los datos
    tabla = 'estudiante'

    # Insertar los datos en la tabla
    insercion.insertar_datos(tabla, datos)

    # Desconectar al finalizar
    insercion.desconectar()